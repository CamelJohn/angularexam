import { AngularFirestore } from 'angularfire2/firestore';
import { Orders } from './../../interfaces/orders.model';
import { FormComponent } from './../form/form.component';
import { FifoService } from './../../shared/fifo.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit, ViewChild, Input } from '@angular/core';

@Component({
  selector: 'app-fifo',
  templateUrl: './fifo.component.html',
  styleUrls: ['./fifo.component.css']
})
export class FifoComponent implements OnInit {
  displayedColumns: string[] = ['position', 'orderNum', 'name', 'weight', 'symbol'];
  // displayedColumns: string[] = ['Customer name', 'orderNum', 'delivery date', 'preferencs'];
  // dataSource = new MatTableDataSource<PeriodicEelement>(ELEMENT_DATA);

  dataSource = new MatTableDataSource<Orders>();

  @ViewChild(MatPaginator, { static: true}) paginator: MatPaginator;

  constructor(
    private fifo: FifoService,
    private form: FormComponent,
    private db: AngularFirestore
    ) { }
  // @Input() orders = this.fifo.getAllOrders();

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

}

// export interface PeriodicEelement {
//   name: string;
//   orderNum: number;
//   position: number;
//   weight: number;
//   symbol: string;
// }

// const ELEMENT_DATA: PeriodicEelement[] = [
//   // {position: 1, orderNum: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   // {position: 2, orderNum: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   // {position: 3, orderNum: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   // {position: 4, orderNum: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   // {position: 5, orderNum: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
//   // {position: 6, orderNum: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   // {position: 7, orderNum: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   // {position: 8, orderNum: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   // {position: 9, orderNum: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   // {position: 10, orderNum: 10 , name: 'Neon', weight: 20.1797, symbol: 'Ne'},
//   // {position: 11, orderNum: 11 , name: 'Sodium', weight: 22.9897, symbol: 'Na'},
//   // {position: 12, orderNum: 12 , name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
//   // {position: 13, orderNum: 13 , name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
//   // {position: 14, orderNum: 14 , name: 'Silicon', weight: 28.0855, symbol: 'Si'},
//   // {position: 15, orderNum: 15 , name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
//   // {position: 16, orderNum: 16 , name: 'Sulfur', weight: 32.065, symbol: 'S'},
//   // {position: 17, orderNum: 17 , name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
//   // {position: 18, orderNum: 18 , name: 'Argon', weight: 39.948, symbol: 'Ar'},
//   // {position: 19, orderNum: 19 , name: 'Potassium', weight: 39.0983, symbol: 'K'},
//   // {position: 20, orderNum: 20 , name: 'Calcium', weight: 40.078, symbol: 'Ca'},
// ];

