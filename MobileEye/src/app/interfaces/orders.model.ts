export interface Orders {
  customerName: string;
  orderNumber: number;
  shirtType: string;
  bodyColor: string;
  sleeveColor?: string;
  preferences: string;
  orderQuantity: number;
  extrafeatures?: string[];
  deliveryRangeInDay: number;
  receivedPrderDate: Date;
  orderToDelivery: number;
  tier: string;
}
