import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { OrdersFormComponent } from './orders-form/orders-form.component';
import { TDordersFormComponent } from './tdorders-form/tdorders-form.component';
import { ReactiveOrdersFormComponent } from './reactive-orders-form/reactive-orders-form.component';

@NgModule({
  declarations: [
    AppComponent,
    OrdersFormComponent,
    TDordersFormComponent,
    ReactiveOrdersFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
