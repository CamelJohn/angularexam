// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDVmi-OActn_sVhTfxcgszcoI5_-rJwvS0',
    authDomain: 'angularclasswork.firebaseapp.com',
    databaseURL: 'https://angularclasswork.firebaseio.com',
    projectId: 'angularclasswork',
    storageBucket: 'angularclasswork.appspot.com',
    messagingSenderId: '156262739401',
    appId: '1:156262739401:web:47e9cee5c14c933fd64c18',
    measurementId: 'G-GQE1XQC2L2'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
