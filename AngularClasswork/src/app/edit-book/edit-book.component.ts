import { BooksService } from './../books.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {
book: string;
author: string;
key;

  constructor(private router: Router,
              private booksService: BooksService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.book = this.route.snapshot.params.title;
    this.author = this.route.snapshot.params.author;
    this.key = this.route.snapshot.params.key;
  }

  onSubmit() {
    // console.log(this.key)
    this.booksService.editBook(this.book, this.author, this.key);
    this.router.navigate(['/books']);
  }

}
