import { Observable } from 'rxjs';
import { BooksService } from './../books.service';
import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})

export class BooksComponent implements OnInit {

  @Output() panelOpenState = false;
  // books: any;
  books$: Observable<any[]>;

  constructor(private booksService: BooksService) { }

  ngOnInit() {
    // this.books = this.booksService.getBooks().subscribe(
    //   (books) => this.books = books
    // );
    // this.booksService.addBooks();
    this.books$ = this.booksService.getBooks();
  }

  deleteBook(key: any) {
    this.booksService.deleteBook(key);
  }

}
