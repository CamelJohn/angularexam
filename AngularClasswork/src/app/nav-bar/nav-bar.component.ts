import { AuthService } from './../auth.service';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent {
  title: string;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              public authService: AuthService,
              private router: Router,
              private location: Location) {
                router.events.subscribe(val => {
                  if (location.path() === '/books') {
                    this.title = 'Books';
                  } else if (location.path() === '/tempForm') {
                    this.title = 'Temperature Form';
                  } else if (location.path() === '/add-books') {
                    this.title = 'Add books';
                  } else if (location.path().endsWith('edit')) {
                    this.title = 'Edit book mode';
                  } else if (location.path() === '/sign-up') {
                    this.title = 'Sign up';
                  } else if (location.path() === '/log-in') {
                    this.title = 'Log In';
                  } else {
                    this.title = 'Temperatures';
                  }
                });
              }
}
