import { map, catchError } from 'rxjs/operators';
import { WeatherRaw } from './interfaces/weather-raw';
import { Weather } from './interfaces/weather';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TempService {
  private URL = 'http://api.openweathermap.org/data/2.5/weather?q=';
  private KEY = 'af1bc660b679df8c50446f92d9d35223';
  private IMP = '&units=metric';

  searchWeatherData(cityName: string): Observable<Weather> {
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
      .pipe(
        map(data => this.transformWeatherData(data)),
        catchError(this.handleError)
      );
  }

  constructor(private http: HttpClient) { }

  private handleError(res: HttpErrorResponse) {
    console.log(res.error);
    return throwError(res.error);
  }

  private transformWeatherData(data: WeatherRaw): Weather {
    return {
      name: data.name,
      country: data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description: data.weather[0].description,
      temperature: data.main.temp,
      lat: data.coord.lat,
      lon: data.coord.lon,
      cod: data.cod
    };
  }

}

