import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { EditBookComponent } from './edit-book/edit-book.component';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { BookFormComponent } from './book-form/book-form.component';
import { environment } from './../environments/environment';
import { AngularFireModule } from '@angular/fire';

import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';

import { AppComponent } from './app.component';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent } from './books/books.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { MatCardModule } from '@angular/material/card';
import { AppRouterModule } from './app-routes.module';
import { TempformComponent } from './tempForm/tempForm.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule, MatInputModule} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LogInComponent } from './log-in/log-in.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    BooksComponent,
    TemperaturesComponent,
    TempformComponent,
    BookFormComponent,
    EditBookComponent,
    SignUpComponent,
    LogInComponent
   ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    RouterModule,
    AppRouterModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule
   ],
  providers: [AngularFirestore, AngularFireAuth ],
  bootstrap: [AppComponent]
})
export class AppModule { }
