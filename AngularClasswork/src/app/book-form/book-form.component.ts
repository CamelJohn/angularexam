import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {
  author: string;
  book: string;
  key;

  constructor(private router: Router,
              private booksService: BooksService,
              public authService: AuthService) { }

  onSubmit() {
    this.authService.user.subscribe(user => {
      this.key = user.uid;
    })
    console.log(this.book, this.author)
    this.pushBook(this.book, this.author);

    this.router.navigate(['/books']);
  }

  ngOnInit() {
    
  }

  pushBook(book: string, author: string) {
    this.booksService.addBooks(book, author);
  }
}
