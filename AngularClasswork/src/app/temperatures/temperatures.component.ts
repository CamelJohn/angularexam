import { TempService } from './../temp.service';
import { Weather } from './../interfaces/weather';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  counter: number = 0;
  temperatue;
  city;
  image: String;
  tempdata$: Observable<Weather>;
  errorMessage: string;
  errorCode: number;
  hasError: Boolean = false;

  constructor(private route: ActivatedRoute,
              private tempService: TempService) { }

  ngOnInit() {
    // this.temperatue = this.route.snapshot.params.temp;
    this.city = this.route.snapshot.params.city;
    this.tempdata$ = this.tempService.searchWeatherData(this.city);
    this.tempdata$.subscribe(
      data => {
        // console.log(data);
        console.dir(data);
        this.temperatue = data.temperature;
        this.image = data.image;
      },
      error => {
        this.hasError = true;
        this.errorMessage = error.message;
        this.errorCode = error.cod;
        console.log('in the temperatures component, { error message:' + error.message + '}');
      }
    );

  }

  onClickLike() {
    this.counter += 1;
    }
}
