// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDLPQHUqgZHzHT_r8oJreP2QyJZ0gPvHbA',
    authDomain: 'angularmaterialcourse.firebaseapp.com',
    databaseURL: 'https://angularmaterialcourse.firebaseio.com',
    projectId: 'angularmaterialcourse',
    storageBucket: 'angularmaterialcourse.appspot.com',
    messagingSenderId: '49671467631'
  }
};
