import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './../shared/auth.service';
import { PostsService } from './../shared/posts.service';
import { Post } from './../shared/posts';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})
export class BlogPostsComponent implements OnInit {
  panelOpenState = false;
  posts$:Observable<Post[]>;
  laterviewing = false;
  currentPostId;
  clickedPostsArray = [];
  updatedPosts;
  constructor(private postsService: PostsService, private db: AngularFirestore, private authservice: AuthService ) { }

  ngOnInit() {
    this.posts$ = this.postsService.getPostsData();
    this.updatedPosts = this.db.collection('posts');
    // console.log(this.posts$);
  }

  check(id) {
    let res = false;
    for (let i = 0; i < this.clickedPostsArray.length; i++) {
      if (id === this.clickedPostsArray[i]) {
        res = true;
    } else {
      res = false;
    }
    return res;
  }
}
//   addPPostsToFirestore(){
//     this.postsService.addToFirestore();

// }
savePost(id, title, body) {
  this.laterviewing = true;
  this.clickedPostsArray.push(id);
  console.log(this.clickedPostsArray)
  this.currentPostId = id;
  let msg;
  const email = this.authservice.currentUserEmail;
  
  if (this.laterviewing) {
    msg = "Saved for later viewing";
  } else {
    msg = '';
  }
    this.db.collection('posts').add({postId: id, postTitle: title, postbody: body, owner: email, message: msg });
  }

  getPosts(id) {
    const post = this.db.collection('posts', ref => ref.where('id', '==' , id));
    return post;
  }
}
