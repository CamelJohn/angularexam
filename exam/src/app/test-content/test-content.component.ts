import { AuthService } from './../shared/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-content',
  templateUrl: './test-content.component.html',
  styleUrls: ['./test-content.component.css']
})
export class TestContentComponent implements OnInit {

  constructor(private authService: AuthService) { }
  email = this.authService.getcurrentUserData();
  
  ngOnInit() {

  }

}
