import { SavedBlogPostComponent } from './saved-blog-post/saved-blog-post.component';
import { BlogPostsComponent } from './blog-posts/blog-posts.component';
import { FormComponent } from './classification/form/form.component';
import { AuthGuard } from './shared/auth.guard';
import { TestContentComponent } from './test-content/test-content.component';
import { SignupComponent } from './auth/signup/signup.component';
import { LoginComponent } from './auth/login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResultComponent } from './classification/result/result.component';
import { PastComponent } from './classification/past/past.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'content', component: TestContentComponent, canActivate: [AuthGuard] },
  { path: 'classificationForm', component: FormComponent, canActivate: [AuthGuard] },
  { path: 'classificationResult', component: ResultComponent, canActivate: [AuthGuard] },
  { path: 'classificationPast', component: PastComponent, canActivate: [AuthGuard] },
  { path: 'blogpost', component: BlogPostsComponent, canActivate: [AuthGuard] },
  { path: 'savedblogpost', component: SavedBlogPostComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
