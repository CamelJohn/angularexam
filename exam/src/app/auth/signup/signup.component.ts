import { AuthService } from './../../shared/auth.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/user.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['.././login/login.component.css']
})
export class SignupComponent implements OnInit {
  pwdToggle = 'password';
  // key;
  // user: Observable<User | null>;


  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  togglePwd() {
    if (this.pwdToggle === 'password') {
      this.pwdToggle = 'text';
    } else {
      this.pwdToggle = 'password';
    }
  }

  onSubmit(form: NgForm) {
    // console.log(form);
    this.authService.registerUser(form.value.email, form.value.password);
  }
}
