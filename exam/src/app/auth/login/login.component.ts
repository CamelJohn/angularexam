import { AuthService } from './../../shared/auth.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  pwdToggle = 'password';

  togglePwd() {
    if (this.pwdToggle === 'password') {
      this.pwdToggle = 'text';
    } else {
      this.pwdToggle = 'password';
    }
  }
  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.authService.login(form.value.email, form.value.password);
  }
}
