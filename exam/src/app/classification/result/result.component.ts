import { Component, OnInit } from '@angular/core';
import { ImageService } from 'src/app/shared/image.service';
import { ClassificationService } from 'src/app/shared/classification.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 

  constructor(
    public classifyService: ClassificationService,
    public imageService: ImageService
    ) {}

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
      }
    )
  }

}
