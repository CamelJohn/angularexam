import { ImageService } from './../../shared/image.service';
import { ClassificationService } from './../../shared/classification.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  url:string;
  text:string;
  
  onSubmit() {
    this.classifyService.doc = this.text;
    this.router.navigate(['/classificationResult']);
  }
  constructor(
    private classifyService: ClassificationService, 
    public imageService:ImageService,
    private router:Router
    ) { }

  ngOnInit() {
  }

}
