import { Post } from './../shared/posts';
import { Observable } from 'rxjs';
import { AuthService } from './../shared/auth.service';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';
import { PostsService } from '../shared/posts.service';
import { ConstantPool } from '@angular/compiler';

@Component({
  selector: 'app-saved-blog-post',
  templateUrl: './saved-blog-post.component.html',
  styleUrls: ['./saved-blog-post.component.css']
})
export class SavedBlogPostComponent implements OnInit {

  constructor(private postsService: PostsService, private db: AngularFirestore, private authservice: AuthService ) { 
    this.fillteredPosts = db.collection<Post>('posts');
    this.onwer = this.authservice.getcurrentUserData();
  }
  panelOpenState = false;
  posts$:Observable<Post[]>;
  currentPostId;
  fillteredPosts;
  fillteredPosts1;
  filter;
  onwer;
  currentLike = 0;
  isDeleted = false;
  currentDeletedPosts = [];
  
  allPosts: Observable<Post[]>;

  ngOnInit() {
    this.posts$ = this.postsService.getPostsData();
    this.onwer.toString();
    this.filter = this.db.collection('posts', ref => ref.where('owner', '==', this.onwer)).valueChanges({ idField: 'key'});
    this.fillteredPosts1 = this.filterPosts(this.filter);

    console.log(this.postsService.getComments());
  }

  onDelete(key, id) {
    this.db.collection('posts').doc(key).delete();
    this.isDeleted = true;
    this.currentDeletedPosts.push(id);
  }
  
  currentLikes(id) {
    this.fillteredPosts1.forEach(post => {
      if (post.id === id) {
        return post.like;
      } else {
        return 0;
      }
    });
  }

  onLike(id, key) {
    this.currentLike = this.currentLike + 1;
    console.log(this.currentLike);
    console.log(key);
    this.db.collection('posts').doc(key).set({like: this.currentLike})
  }

  filterPosts(filter) {
    const postsArray = []
    filter.forEach(doc => {
      doc.forEach(docEl => {
          postsArray.push(docEl);
      });
    });
    return postsArray;
  }

  // getUserPosts() {
  //   const postsRef = this.db.collection('posts').valueChanges();
  //   return postsRef;
  // }

}
