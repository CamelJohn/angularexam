import { PostsService } from './shared/posts.service';
import { MaterialModule } from './material.module';
import { environment } from './../environments/environment';
import { AuthService } from './shared/auth.service';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { TestContentComponent } from './test-content/test-content.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage';
import { FormComponent } from './classification/form/form.component';
import { ResultComponent } from './classification/result/result.component';
import { PastComponent } from './classification/past/past.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { BlogPostsComponent } from './blog-posts/blog-posts.component';
import { SavedBlogPostComponent } from './saved-blog-post/saved-blog-post.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    SignupComponent,
    WelcomeComponent,
    TestContentComponent,
    FormComponent,
    ResultComponent,
    PastComponent,
    BlogPostsComponent,
    SavedBlogPostComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule  ,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    FlexLayoutModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [AuthService, AngularFirestore, AngularFireAuth, PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
