export interface Post {
    id:number,
    userId: number,
    title: string,
    body: string,
    // userName: string,
    message?: string,
    comment?: string,
    like?: number,
}
