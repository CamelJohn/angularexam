import { Comment } from './comment';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Post } from './posts';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
      private POSTURL = "https://jsonplaceholder.typicode.com/posts";
      private commentsUrl = 'https://jsonplaceholder.typicode.com/comments';
      private posts: Observable<Post[]>;
      private itemsCollection: AngularFirestoreCollection<Post>;
      private comments: Observable<Comment[]>;
  
      constructor(private http: HttpClient, private db: AngularFirestore){
          this.itemsCollection = db.collection<Post>('posts');
      }
  
      getAllUsers(){
        //   const users = this.http.get<User[]>(`${this.USERSURL}`);
          const users = this.db.collection('users').valueChanges();
          return users;
      }

      getComments() {
          const res = this.db.collection('comments').valueChanges();
          const ans = this.filterComments(res);
          return  ans;
      }

      filterComments(filter) {
        const postsArray = []
        filter.forEach(doc => {
          doc.forEach(docEl => {
              postsArray.push(docEl);
          });
        });
        return postsArray;
      }


      getPostsData(): Observable<Post[]>{
          this.posts = this.http.get<Post[]>(`${this.POSTURL}`).pipe(
              map((data) => 
              this.addUsersToPosts(data)),
              catchError(this.handleError));
          return this.posts;
      }
  
      private handleError(res: HttpErrorResponse) {
          console.log(res.error);
          return throwError(res.error);
        }
  
      addUsersToPosts(data: Post[]): Post[]{
          const users = this.getAllUsers();
          const postsArray = []
          users.forEach(user => {    
              user.forEach(u =>{
                  data.forEach(post =>{
                    //   if(post.userId === u.id){
                          postsArray.push({
                              id: post.id,
                              userId:post.userId,
                              title: post.title,
                              body: post.body,
                              comment: post.comment
                              // userName: u.name
                          })
                    //   }
                  })
              })
          })
      return postsArray;
      }
  
      addToFirestore(){
          const users = this.getAllUsers();
          const posts = this.http.get<Post[]>(`${this.POSTURL}`);
          const comments = this.http.get<Comment[]>(`${this.commentsUrl}`);
        //   const comments = this.comments;
          let items: Observable<Post[]>;
          items = this.itemsCollection.valueChanges();
  
          items.forEach(data => {
              if(data.length === 0){
                  posts.forEach(post =>{
                      post.forEach(postEl => {
                          users.forEach(user => {
                              user.forEach(el => {
                                comments.forEach(comment => {
                                   comment.forEach(commentEl => {
                                    if (commentEl.postId === postEl.id) {
                                        this.db.collection("posts").add({
                                            title: postEl.title,
                                            body: postEl.body,
                                            comment: commentEl.body
                                        })
                                    }
                                   })
                                })
                              })
                          })
                      })
                  })
              }
              else{
                  
              }
          })
      }
  }