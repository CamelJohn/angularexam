import { AngularFirestore } from '@angular/fire/firestore';
import { User } from './user.model';
import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class AuthService {
  authChange = new Subject<boolean>();
  user: Observable<User | null>;
  isAuthenticated = false;
  currentUserEmail: string;

  constructor(
    private router: Router,
    public afAuth: AngularFireAuth,
    private snackbar: MatSnackBar,
    private db: AngularFirestore
    ) {
      this.user = this.afAuth.authState;
    }

  getcurrentUserData() {
    return this.currentUserEmail;
  }

  initAuthListener(route: string = '') {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.currentUserEmail = user.email;
        console.log(this.currentUserEmail);
        this.isAuthenticated = true;
        this.authChange.next(true);
        this.router.navigate([`/${route}`]);
      } else {
        this.afAuth.auth.signOut();
        this.isAuthenticated = false;
        this.authChange.next(false);
        this.router.navigate([`/welcome`]);
      }
    });
  }

  registerUser(email: string, password: string) {
    // console.log(email, password);
    this.afAuth.auth.createUserWithEmailAndPassword(
      email, password
      ).then(result => {
        console.log(result);
        this.initAuthListener('login');
      })
      .catch(error => {
        this.snackbarServer(error.message);
      });
  }

  login(email: string, password: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password)
    .then(res => {
      this.snackbarServer('Login was successfull');
      this.initAuthListener('content');
    })
    .catch(error => {
      this.snackbarServer(error.message);
    });
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  getUser() {
    return {...this.user };
  }

  isAuth() {
    return this.isAuthenticated = true;
  }

  snackbarServer(message: string) {
    this.snackbar.open(message, null, {
      duration: 10000,
      panelClass: 'success'
    });
  }
}
