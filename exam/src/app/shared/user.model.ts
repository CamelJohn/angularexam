export interface User {
  uid: string;
  // id: number,
  email?: string | null;
  photoUrl?: string;
  displayName?: string;
}
