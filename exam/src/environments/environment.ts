// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCMvw8C80Y7-N4jI1X09PVBKp7L2wQU4mw",
  authDomain: "exam-5ebcb.firebaseapp.com",
  databaseURL: "https://exam-5ebcb.firebaseio.com",
  projectId: "exam-5ebcb",
  storageBucket: "exam-5ebcb.appspot.com",
  messagingSenderId: "310285282247",
  appId: "1:310285282247:web:fd68aa9165ca44a63347e2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
